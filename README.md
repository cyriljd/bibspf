# bibspf

*bibspf* est un style bibliographique pour Biblatex qui suit les normes du Bulletin de la Société préhistorique française.  
Il s'agit d'une modification des styles *authoryear-comp* pour les citations et *authoryear* pour la bibliographie.

## Installation ##

Le style *bibspf* est prévu pour être utilisé avec *biblatex* et *biber*.  
Voici la marche à suivre:
* téléchargez l'archive https://framagit.org/jd/bibspf/-/archive/master/bibspf-master.zip
* décompressez *bibspf-master.zip*
* déplacez le dossier *bibspf* dans un des dossiers accessibles par votre distribution latex, par exemple`[...]/texmf-var/tex/`.  

## Utilisation

Dans un premier temps, assurez vous d'avoir initialisé *biblatex* avec *biber*:

```tex
\usepackage[]{bibspf}
```

Commentez les lignes avec le caractère `%` selon ce que vous désirez.

Pour une configuration plus avancée et voir comment bien remplir sa base bibliographique, allez voir la [documentation](https://framagit.org/jd/biblatex-spf/wikis/home).

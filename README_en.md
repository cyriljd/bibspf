# bibspf
*bibspf* is a bibliographic style for *biblatex* that follows the standards of the Bulletin de la Société préhistorique française.
This is a modification of the *authoryear-comp* styles for citations and *authoryear* for bibliography.

## Installation
The *bibspf* style is intended to be used with *biblatex* and *biber*.
Here is the procedure to follow:

* download the archive https://framagit.org/jd/bibspf/-/archive/master/bibspf-master.zip
* decompress *bibspf-master.zip*
* move the *bibspf* folder to one of the folders accessible by your latex distribution, for example `[...]/texmf-var/tex/`.

## Usage
First, make sure you have initialized *biblatex* with *biber*:

```tex
\usepackage[]{bibspf}
```

Comment the lines with the `%` character according to what you want.

For a more advanced configuration and to see how to fill its bibliographic base well, go to the documentation.
